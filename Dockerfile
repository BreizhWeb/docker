# Utilisation de l'image PHP 8.2 avec Apache
FROM php:8.2-apache

# Installation des dépendances nécessaires
RUN apt-get update && apt-get install -y \
    libzip-dev \
    zip \
    unzip \
    git \
    libcurl4-openssl-dev \
    pkg-config \
    libssl-dev \
    curl

# Activation des modules Apache nécessaires
RUN a2enmod rewrite

# Installation des extensions PHP
RUN docker-php-ext-install pdo_mysql

# Copie des fichiers de l'application dans le conteneur
COPY . /var/www/html

# Définition du répertoire de travail
WORKDIR /var/www/html

# Copie du fichier .env dans le conteneur
COPY .env.example .env

# Installation de Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Nettoyage du cache de Composer
RUN composer clear-cache

# Installation des dépendances PHP via Composer
RUN composer install --ignore-platform-reqs

# Génération de la clé d'application
RUN php artisan key:generate

# Exposition du port 80 (port par défaut d'Apache)
EXPOSE 80

# Commande à exécuter lors du démarrage du conteneur
CMD ["php", "artisan", "serve", "--host", "0.0.0.0"]
